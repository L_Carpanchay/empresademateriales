package empresaDeMateriales.empresa;

import org.junit.Before;
import empresaDeMateriales.excepciones.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;

public class OrdenDeVentaTest {
    OrdenDeVenta ticket;
    @Before
    public void before(){
        ticket = new OrdenDeVenta(1);
    }

    @Test
    public void agregarItemTest() throws ItemExistenteException{
        Item item = new HerramientaElectrica("TALADRO", 123L, 150F, "Hacer agujeros", 7.5F);
        Item item2 = new HerramientaElectrica("SierraElectrica", 789L, 150F, "Cortar", 10F);
        Item item3 = new HerramientaElectrica("Voltimetro", 456L, 700F, "Medir voltaje", 5.5F);
        ticket.agregarItem((ItemDeVenta)item);
        ticket.agregarItem((ItemDeVenta)item2);
        ticket.agregarItem((ItemDeVenta)item3);
        ArrayList<ItemDeVenta> listaItems = new ArrayList<ItemDeVenta>();
        listaItems.add((ItemDeVenta)item);
        listaItems.add((ItemDeVenta)item2);
        listaItems.add((ItemDeVenta)item3);
        assertEquals(listaItems, ticket.getItemsVendidos());
    }

    @Test
    public void modificarItemTest()throws ItemExistenteException, ItemInexistenteException{
        Item item = new HerramientaElectrica("TALADRO", 123L, 150F, "Hacer agujeros", 7.5F);
        ticket.agregarItem((ItemDeVenta)item);
        Item itemModificado = new HerramientaElectrica("Martillo", 123L, 175F, "Golpear", 7F);
        ArrayList<ItemDeVenta> listaItems = new ArrayList<ItemDeVenta>();
        listaItems.add((ItemDeVenta)itemModificado);
        ticket.modificarItemVendido((ItemDeVenta)itemModificado);
        assertEquals(listaItems, ticket.getItemsVendidos());
    }

    @Test
    public void eliminarItemTest()throws ItemExistenteException, ItemInexistenteException{
        Item item = new HerramientaElectrica("TALADRO", 123L, 150F, "Hacer agujeros", 7.5F);
        Item item2 = new HerramientaElectrica("SierraElectrica", 789L, 150F, "Cortar", 10F);
        Item item3 = new HerramientaElectrica("Voltimetro", 456L, 700F, "Medir voltaje", 5.5F);
        ticket.agregarItem((ItemDeVenta)item);
        ticket.agregarItem((ItemDeVenta)item2);
        ticket.agregarItem((ItemDeVenta)item3);
        ticket.eliminarItemVendoble(789L);
        ArrayList<ItemDeVenta> listaItems = new ArrayList<ItemDeVenta>();
        listaItems.add((ItemDeVenta)item);
        listaItems.add((ItemDeVenta)item3);
        assertEquals(listaItems, ticket.getItemsVendidos());
    }
}
