package empresaDeMateriales.empresa;

import java.util.ArrayList;

import org.junit.Before;

import empresaDeMateriales.empresa.Empresa;
import empresaDeMateriales.empresa.Item;
import empresaDeMateriales.excepciones.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class EmpresaTest{
    Empresa gestor;
    @Before
    public void before(){
        gestor = new Empresa();
    }

    @Test
    public void agregarItemTest() throws ItemExistenteException{
        Item item = new HerramientaElectrica("TALADRO", 123L, 150F, "Hacer agujeros", 7.5F);
        gestor.agregarItem(item);
        ArrayList<Item> listaItems = new ArrayList<Item>();
        listaItems.add(item);
        assertEquals(listaItems, gestor.getListadoDeItems());
    }

    @Test
    public void modificarItemTest()throws ItemExistenteException, ItemInexistenteException{
        Item item = new HerramientaElectrica("TALADRO", 123L, 150F, "Hacer agujeros", 7.5F);
        gestor.agregarItem(item);
        Item itemModificado = new HerramientaElectrica("TALADRO", 123L, 250F, "Hacer agujeros", 10F);
        gestor.modificarItem(itemModificado);
        assertEquals(itemModificado, gestor.getItem(123L));
    }

    @Test
    public void eliminarItemTest()throws ItemExistenteException, ItemInexistenteException{
        Item item = new HerramientaElectrica("TALADRO", 123L, 150F, "Hacer agujeros", 7.5F);
        Item item2 = new HerramientaElectrica("SierraElectrica", 789L, 150F, "Cortar", 10F);
        Item item3 = new HerramientaElectrica("Voltimetro", 456L, 700F, "Medir voltaje", 5.5F);
        gestor.agregarItem(item);
        gestor.agregarItem(item2);
        gestor.agregarItem(item3);
        gestor.eliminarItem(789L);
        ArrayList<Item> listaItems = new ArrayList<Item>();
        listaItems.add(item);
        listaItems.add(item3);
        assertEquals(listaItems, gestor.getListadoDeItems());
    }

    @Test
    public void ordenarItemsPorCodigoTest(){
        Item item = new HerramientaElectrica("TALADRO", 123L, 150F, "Hacer agujeros", 7.5F);
        Item item2 = new HerramientaElectrica("SierraElectrica", 789L, 150F, "Cortar", 10F);
        Item item3 = new HerramientaElectrica("Voltimetro", 456L, 700F, "Medir voltaje", 5.5F);
        Proveedor proveedor = new Proveedor("Casas", "Mitre 450", "Argentina", "Salta");
        Item item4 = new MateriaPrima("Madera", 159L, 900F, proveedor);
        gestor.agregarItem(item);
        gestor.agregarItem(item2);
        gestor.agregarItem(item3);
        gestor.agregarItem(item4);
        ArrayList<Item> listaItems = new ArrayList<Item>();
        listaItems.add(item);
        listaItems.add(item3);
        listaItems.add(item2);
        assertEquals(listaItems, gestor.ordenarItemsPorCodigo());
    }

    @Test
    public void ordenarItemsPorPrecioTest(){
        Item item = new HerramientaElectrica("TALADRO", 123L, 170F, "Hacer agujeros", 7.5F);
        Item item2 = new HerramientaElectrica("SierraElectrica", 789L, 150F, "Cortar", 10F);
        Item item3 = new HerramientaElectrica("Voltimetro", 456L, 700F, "Medir voltaje", 5.5F);
        Proveedor proveedor = new Proveedor("Casas", "Mitre 450", "Argentina", "Salta");
        Item item4 = new MateriaPrima("Madera", 159L, 900F, proveedor);
        gestor.agregarItem(item);
        gestor.agregarItem(item2);
        gestor.agregarItem(item3);
        gestor.agregarItem(item4);
        ArrayList<Item> listaItems = new ArrayList<Item>();
        listaItems.add(item2);
        listaItems.add(item);
        listaItems.add(item3);
        assertEquals(listaItems, gestor.ordenarItemsPorPrecio());
    }

    @Test
    public void agregarTicketsTest() throws ItemExistenteException{
        OrdenDeVenta ticket = new OrdenDeVenta(1);
        Item item = new HerramientaElectrica("TALADRO", 123L, 170F, "Hacer agujeros", 7.5F);
        Item item2 = new HerramientaElectrica("SierraElectrica", 789L, 150F, "Cortar", 10F);
        Item item3 = new HerramientaElectrica("Voltimetro", 456L, 700F, "Medir voltaje", 5.5F);
        ticket.agregarItem((ItemDeVenta)item);
        ticket.agregarItem((ItemDeVenta)item2);
        ticket.agregarItem((ItemDeVenta)item3);
        gestor.agregarTickets(ticket);
        ArrayList<OrdenDeVenta> tickets = new ArrayList<OrdenDeVenta>();
        tickets.add(ticket);
        assertEquals(tickets, gestor.getListadoDetickets());
    }
}