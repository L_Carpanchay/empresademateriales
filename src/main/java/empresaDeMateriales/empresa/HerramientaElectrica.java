package empresaDeMateriales.empresa;

public class HerramientaElectrica extends Herramienta{
    // atributos
    private Float consumoElectrico;

    // constructores
    public HerramientaElectrica(String nombre, Long codigo, Float precio, String funcionalidad, Float consumoElectrico) {
        super(nombre, codigo, precio, funcionalidad);
        this.consumoElectrico = consumoElectrico;
    }

    // getters y setters
    public Float getConsumoElectrico() {
        return consumoElectrico;
    }

    public void setConsumoElectrico(Float consumoElectrico) {
        this.consumoElectrico = consumoElectrico;
    }

    //metodos
    
    @Override
    public String toString() {
        return "Nombre: " + getNombre() + ", Codigo: " + getCodigo() + ", Precio: " + getPrecio() + ", Funcionalidad: " + getFuncionalidad() + ", Consumo Electrico: " + consumoElectrico;
    }
}
