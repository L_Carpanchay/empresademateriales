package empresaDeMateriales.empresa;

public class MateriaPrima extends Material {

    // atributos
    private Proveedor proveedor;

    // constructores
    public MateriaPrima(String nombre, Long codigo, Float precio, Proveedor proveedor) {
        super(nombre, codigo, precio);
        this.proveedor = proveedor;
    }

    // getters y setters
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
    // metodos
    @Override
    public void mostrarDatos(){
        System.out.println("Material: "+getNombre()+" Codigo: "+getCodigo());
        System.out.println("Precio: "+getPrecio());
    }
}
