package empresaDeMateriales.empresa;
import java.util.Comparator;

public class ComparadorPorCodigo implements Comparator{
    @Override
    public int compare(Object o1, Object o2) {
        Item item1 = (Item) o1;
        Item item2 = (Item) o2;
        return item1.getCodigo().compareTo(item2.getCodigo());
    }
}
