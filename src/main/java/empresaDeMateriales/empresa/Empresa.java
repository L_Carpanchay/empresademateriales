package empresaDeMateriales.empresa;
import empresaDeMateriales.excepciones.ItemInexistenteException;
import empresaDeMateriales.excepciones.OrdenInexistenteException;
import empresaDeMateriales.excepciones.OrdenVaciaException;
import empresaDeMateriales.excepciones.ItemExistenteException;
import java.util.ArrayList;
import java.util.Collections;

public class Empresa {

    // atributos
    private ArrayList<OrdenDeVenta> listadoDetickets;
    private ArrayList<Item> listadoDeItems;

    // constructores
    public Empresa() {
        listadoDeItems = new ArrayList<Item>();
        listadoDetickets = new ArrayList<OrdenDeVenta>();
    }

    // getters
    public ArrayList<Item> getListadoDeItems() {
        return listadoDeItems;
    }

    public ArrayList<OrdenDeVenta> getListadoDetickets() {
        return listadoDetickets;
    }

    public OrdenDeVenta getOrdenDeVenta(Integer numeroDeOrden) throws OrdenInexistenteException{
        OrdenDeVenta ordenDeVentaEncontrada = null;
        for(OrdenDeVenta var: listadoDetickets){
            if(var.getNumeroDeOrden().equals(numeroDeOrden)){
                ordenDeVentaEncontrada = var;
            }
        }
        if(ordenDeVentaEncontrada == null){
            throw new OrdenInexistenteException();
        }
        return ordenDeVentaEncontrada;
    }

    public Item getItem(Long codigo)throws ItemInexistenteException{
        Item itemEncontrado = null;
        for(Item var: listadoDeItems){
            if(var.getCodigo().equals(codigo)){
                itemEncontrado = var;
            }
        }
        if(itemEncontrado == null){
            throw new ItemInexistenteException();
        }
        return itemEncontrado;
    }
    
    //setters
    public void agregarItem(Item item) throws ItemExistenteException{
        for(Item var: listadoDeItems){
            if(item.getCodigo().equals(var.getCodigo())){
                throw new ItemExistenteException();
            }
        }
        this.listadoDeItems.add(item);
    }

    public void agregarTickets(OrdenDeVenta ticket) throws OrdenVaciaException {
        if(ticket.cantidadDeElementos()==0){
            throw new OrdenVaciaException();
        }
        this.listadoDetickets.add(ticket);
    }

    //metodos
    
    public void modificarItem(Item itemModificado)throws ItemInexistenteException, ItemExistenteException{
        Item itemEncontrado;
        itemEncontrado = getItem(itemModificado.getCodigo());
        listadoDeItems.remove(itemEncontrado);
        this.agregarItem(itemModificado);
    }

    public void eliminarItem(Long codigo) throws ItemInexistenteException{
        Item itemEncontrado;
        itemEncontrado = getItem(codigo);
        listadoDeItems.remove(itemEncontrado);
    }

    public ArrayList<Item> ordenarItemsPorCodigo(){
        ArrayList<Item> listadoDeItemsVendibles = new ArrayList<Item>();
        for(Item var: listadoDeItems){
            if(var.getClass().equals(HerramientaElectrica.class)){
                listadoDeItemsVendibles.add(var);
            }
            if(var.getClass().equals(HerramientaManual.class)){
                listadoDeItemsVendibles.add(var);
            }
            if(var.getClass().equals(MaterialFabricado.class)){
                listadoDeItemsVendibles.add(var);
            }
        }
        Collections.sort(listadoDeItemsVendibles, new ComparadorPorCodigo());
        return listadoDeItemsVendibles;
    }

    public ArrayList<Item> ordenarItemsPorPrecio(){
        ArrayList<Item> listadoDeItemsVendibles = new ArrayList<Item>();
        for(Item var: listadoDeItems){
            if(var.getClass().equals(HerramientaElectrica.class)){
                listadoDeItemsVendibles.add(var);
            }
            if(var.getClass().equals(HerramientaManual.class)){
                listadoDeItemsVendibles.add(var);
            }
            if(var.getClass().equals(MaterialFabricado.class)){
                listadoDeItemsVendibles.add(var);
            }
        }
        Collections.sort(listadoDeItemsVendibles);
        return listadoDeItemsVendibles;
    }
}