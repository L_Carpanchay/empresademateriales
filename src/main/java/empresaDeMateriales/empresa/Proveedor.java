package empresaDeMateriales.empresa;

public class Proveedor {
    private String nombreDelProveedor;
    private String direccion;
    private String pais;
    private String provincia;

    //Constructor
    public Proveedor(String nombreDelProveedor, String direccion, String pais, String provincia) {
        this.nombreDelProveedor = nombreDelProveedor;
        this.direccion = direccion;
        this.pais = pais;
        this.provincia = provincia;
    }

    //getters and setters
    public String getNombreDelProveedor() {
        return nombreDelProveedor;
    }

    public void setNombreDelProveedor(String nombreDelProveedor) {
        this.nombreDelProveedor = nombreDelProveedor;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    
}
