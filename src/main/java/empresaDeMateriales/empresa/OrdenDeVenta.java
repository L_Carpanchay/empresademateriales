package empresaDeMateriales.empresa;

import java.util.ArrayList;
import empresaDeMateriales.excepciones.*;

public class OrdenDeVenta {
    private ArrayList<ItemDeVenta> itemsVendidos;
    private Integer numeroDeOrden;

    //constructor
    public OrdenDeVenta(Integer numeroDeOrden){
        itemsVendidos = new ArrayList<ItemDeVenta>();
        this.numeroDeOrden = numeroDeOrden;
    }

    //getters and setters
    public Integer getNumeroDeOrden() {
        return numeroDeOrden;
    }

    public void setNumeroDeOrden(Integer numeroDeOrden) {
        this.numeroDeOrden = numeroDeOrden;
    }

    public ArrayList<ItemDeVenta> getItemsVendidos(){
        return itemsVendidos;
    }

    public ItemDeVenta getItem(Long codigo){
        ItemDeVenta itemEncontrado = null;
        for(ItemDeVenta var: itemsVendidos){
            if(var.getCodigo().equals(codigo)){
                itemEncontrado=var;
            }
        }
        return itemEncontrado;
    }

    public void modificarItemVendido(ItemDeVenta itemModificado)throws ItemInexistenteException, ItemExistenteException{
        ItemDeVenta itemEncontrado = getItem(itemModificado.getCodigo());
        itemsVendidos.remove(itemEncontrado);
        this.agregarItem(itemModificado);
    }

    public void agregarItem(ItemDeVenta itemVendido) throws ItemExistenteException{
        for(ItemDeVenta var: itemsVendidos){
            if(itemVendido.getCodigo().equals(var.getCodigo())){
                throw new ItemExistenteException();
            }
        }
        this.itemsVendidos.add(itemVendido);
    }

    public void eliminarItemVendoble(Long codigo) throws ItemInexistenteException{
        ItemDeVenta itemEncontrado;
        itemEncontrado = getItem(codigo);
        itemsVendidos.remove(itemEncontrado);
    }

    //metodos
    public int cantidadDeElementos(){
        return itemsVendidos.size();
    }
    public String imprimirOrden(){
        String frase = "Orden De Venta: \n";
        for (ItemDeVenta var : itemsVendidos) {
            String item = "Nombre: "+var.getNombre()+" Codigo: "+var.getCodigo()+" Precio: "+var.getPrecioDeVenta();
            frase = frase + item + "\n";
        }
        return frase;
    }

}