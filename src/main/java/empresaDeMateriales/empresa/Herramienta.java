package empresaDeMateriales.empresa;

public abstract class Herramienta extends Item implements ItemDeVenta{

    
    private String funcionalidad;

    // constructores
    public Herramienta(String nombre, Long codigo, Float precio, String funcionalidad) {
        super(nombre, codigo, precio);
        this.funcionalidad = funcionalidad;
    }

    // getters
    public String getFuncionalidad() {
        return funcionalidad;
    }

    // setters
    public void setFuncionalidad(String funcionalidad) {
        this.funcionalidad = funcionalidad;
    }
    
    // metodos
    @Override
    public Float getPrecioDeVenta(){
        Float ganancia = 0.25F;
        return getPrecio() + (getPrecio()*ganancia);
    }
    
    public void mostrarDatos(){
        System.out.println("Herramienta: "+getNombre()+" Codigo: "+getCodigo()+" Funcionalidad: "+getFuncionalidad());
        System.out.println("Precio: "+getPrecio());
    }
    
}
