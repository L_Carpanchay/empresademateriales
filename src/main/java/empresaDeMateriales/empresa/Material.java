package empresaDeMateriales.empresa;

public abstract class Material extends Item{
    
    //atributos
    private String nombre;
    private Long codigo;
    private Float precio;

    // constructores
    public Material(String nombre, Long codigo, Float precio) {
        super(nombre, codigo, precio);
    }

    public void mostrarDatos(){
        System.out.println("Material: "+getNombre()+" Codigo: "+getCodigo());
        System.out.println("Precio: "+getPrecio());
    }
}