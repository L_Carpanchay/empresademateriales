package empresaDeMateriales.empresa;

public class HerramientaManual extends Herramienta{

    // constructores
    public HerramientaManual(String nombre, Long codigo, Float precio, String funcionalidad) {
        super(nombre, codigo, precio, funcionalidad);
    }

    //metodos

    @Override
    public String toString() {
        return "Nombre: " + getNombre() + ", Codigo: " + getCodigo() + ", Precio: " + getPrecio() + "Funcionalidad: " + getFuncionalidad();
    }
}
