package empresaDeMateriales.empresa;

public interface ItemDeVenta{
    public Float getPrecioDeVenta();
    public String getNombre();
    public Long getCodigo();
    
}
