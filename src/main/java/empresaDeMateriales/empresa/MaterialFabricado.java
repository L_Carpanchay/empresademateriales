package empresaDeMateriales.empresa;

public class MaterialFabricado extends Material implements ItemDeVenta{

    // constructores
    public MaterialFabricado(String nombre, Long codigo, Float precio) {
        super(nombre, codigo, precio);
    }

    // metodos
    @Override
    public Float getPrecioDeVenta(){
        Float ganancia = 0.25F;
        return getPrecio() + (getPrecio()*ganancia);
    }

    @Override
    public void mostrarDatos(){
        System.out.println("Material: "+getNombre()+" Codigo: "+getCodigo());
        System.out.println("Precio: "+getPrecio());
    }

    
    
}