package empresaDeMateriales.empresa;

public abstract class Item implements Comparable<Item>{
    //atributos
    private String nombre;
    private Long codigo;
    private Float precio;
    
    public Item(String nombre, Long codigo, Float precio){
        this.nombre = nombre;
        this.codigo = codigo;
        this.precio = precio;
    }
    public String getNombre() {
        return nombre;
    }

    public Long getCodigo() {
        return codigo;
    }

    public Float getPrecio() {
        return precio;
    }
    //setters
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }
    //metodos
    @Override
    public int compareTo(Item t){
        return precio.compareTo(t.getPrecio());
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre + ", Codigo: " + codigo + ", Precio: " + precio;
    }
}
