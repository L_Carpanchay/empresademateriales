package empresaDeMateriales.empresa;
import empresaDeMateriales.ventana.VentanaPrincipal;
public class Principal {
    public static void main(String[] args){
        Empresa gestor = new Empresa();
        VentanaPrincipal v1 = new VentanaPrincipal(gestor);
        v1.setVisible(true);
    }
}