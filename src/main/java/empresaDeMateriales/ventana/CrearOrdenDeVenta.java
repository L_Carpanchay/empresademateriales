package empresaDeMateriales.ventana;
import empresaDeMateriales.excepciones.CantidadException;
import empresaDeMateriales.excepciones.DataAccessException;
import empresaDeMateriales.excepciones.ItemInexistenteException;
import empresaDeMateriales.excepciones.OrdenVaciaException;
import empresaDeMateriales.empresa.ItemDeVenta;
import empresaDeMateriales.empresa.Empresa;
import empresaDeMateriales.empresa.Item;
import empresaDeMateriales.empresa.OrdenDeVenta;
import javax.swing.JOptionPane;

public class CrearOrdenDeVenta extends javax.swing.JInternalFrame {

    Empresa gestor;
    OrdenDeVenta orden;
    Integer numeroDeOrden;

    public CrearOrdenDeVenta(Empresa gestor, Integer numeroDeOrden) {
        this.gestor = gestor;
        this.numeroDeOrden = numeroDeOrden;
        orden = new OrdenDeVenta(numeroDeOrden);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();

        setClosable(true);
        setTitle("Crear Orden De Venta");

        jButton1.setText("Agregar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Codigo");

        jButton2.setText("Terminar la Creacion de la Orden");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel2.setText("Cantidad");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(57, 57, 57)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel1))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButton2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try{
            Integer cantidad = Integer.parseInt(this.jTextField2.getText());
            errorCantidad(cantidad);
            Long codigo = Long.parseLong(this.jTextField1.getText());
            Item item = gestor.getItem(codigo);
            Integer i = 0;
            for(i = 0; i<cantidad; i++){
                orden.agregarItem((ItemDeVenta) item);
            }
            JOptionPane.showMessageDialog(rootPane,"Item Agregado");
        } catch (DataAccessException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Error",0);
        } catch (ItemInexistenteException ex){
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Error",0);
        } catch (CantidadException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Error",0);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public void errorCantidad(Integer cantidad)throws CantidadException{
        if(cantidad <= 0){
            throw new CantidadException();
        }
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            gestor.agregarTickets(orden);
            JOptionPane.showMessageDialog(rootPane,"Cuenta creada");
        } catch (OrdenVaciaException ex){
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Error",0);
        }
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
