package empresaDeMateriales.excepciones;

public class OrdenInexistenteException extends RuntimeException {
    public OrdenInexistenteException() {
        super("La Orden no existe");
    }
}
