package empresaDeMateriales.excepciones;

public class ItemExistenteException extends RuntimeException {
    public ItemExistenteException() {
        super("El Item ya existe");
    }
}
