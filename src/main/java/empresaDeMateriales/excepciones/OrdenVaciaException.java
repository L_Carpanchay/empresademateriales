package empresaDeMateriales.excepciones;

public class OrdenVaciaException extends RuntimeException{
    public OrdenVaciaException(){
        super("No se ah agregado nada a la orden");
    }
}
