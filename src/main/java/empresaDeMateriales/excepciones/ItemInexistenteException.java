package empresaDeMateriales.excepciones;

public class ItemInexistenteException extends RuntimeException {
    public ItemInexistenteException() {
        super("El Item no existe");
    }
}
